# New parameters for the HTG Gradient

Expose the Divergence, Vorticity and QCriterion when computing the Gradient
of an Hyper Tree Grid. Vector fields are now also supported.
